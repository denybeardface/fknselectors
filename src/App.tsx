import React, { useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";

import "./styles.css";
import Select from "./Select";
import {
  getAllLevels,
  getSelected,
  getFilteredLevels,
} from "./store/select/getters";
import { setSelected, setFilteredLevels } from "./store/select/slice";
import { SelectNode } from "./store/select/types";

const App = () => {
  const dispatch = useDispatch();
  const allLevels = useSelector(getAllLevels);
  const selected = useSelector(getSelected);
  const filteredLevels = useSelector(getFilteredLevels);
  const levels = filteredLevels.length ? filteredLevels : allLevels;

  const handleSelect = (value: SelectNode) => {
    if (value.lvl_code === "") {
      const newSelected = selected.filter(
        (item: SelectNode) => item.lvl !== value.lvl
      );
      dispatch(setSelected(newSelected));
      return;
    }
    dispatch(setSelected([...selected, value]));
  };

  const clearFilter = useCallback(() => {
    dispatch(setFilteredLevels([]));
    dispatch(setSelected([]));
  }, [dispatch]);

  const lvlKey = () => Math.random().toString(36).slice(2);
  return (
    <div className="App">
      {levels.map((level) => {
        return (
          <Select
            key={lvlKey()}
            children={level}
            selected={selected}
            onSelect={handleSelect}
          />
        );
      })}
      {filteredLevels.length > 0 && <button onClick={clearFilter}>Х</button>}
    </div>
  );
};

export default App;
