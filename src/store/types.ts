import { SelectState } from "./select/types";

export interface State {
  select: SelectState;
}
