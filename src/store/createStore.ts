import { Store } from "redux";
import { combineReducers } from "redux";
import { configureStore as createStore } from "@reduxjs/toolkit";
import SelectReducer from "./select/slice";
import createSagaMiddleware from "redux-saga";
import rootSaga from "./rootSaga";

const createRootReducer = () =>
  combineReducers({
    select: SelectReducer
  });

const sagaMiddleware = createSagaMiddleware();

const configureStore = (): Store => {
  const rootReducer = createRootReducer();
  const middlewares = [sagaMiddleware];

  const store = createStore({
    reducer: rootReducer,
    middleware: [...middlewares]
  });
  sagaMiddleware.run(rootSaga);
  return store;
};

export default configureStore;
