import { all } from "redux-saga/effects";

import selectSaga from "./select/saga";

const sagas = [...selectSaga];

export default function* root() {
  yield all(sagas.map((saga) => saga()));
}
