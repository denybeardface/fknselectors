import { createSlice } from "@reduxjs/toolkit";
import { SelectState } from "./types";

const initialState: SelectState = {
  selected: [],
  rawLevels: new Map(),
  allLevels: [],
  filteredLevels: [],
  loading: false,
};

const slice = createSlice({
  name: "select",
  initialState,
  reducers: {
    getLevels(state) {
      state.loading = true;
    },
    setRawLevels(state, action) {
      state.rawLevels = action.payload;
    },
    setAllLevels(state, action) {
      state.loading = false;
      state.allLevels = action.payload;
    },
    setFilteredLevels(state, action) {
      state.filteredLevels = action.payload;
    },
    setSelected(state, action) {
      state.selected = action.payload;
    },
  },
});

export const {
  getLevels,
  setRawLevels,
  setAllLevels,
  setFilteredLevels,
  setSelected,
} = slice.actions;

export default slice.reducer;
