import { State } from "../types";
import { SelectState } from "./types";

const getSelectState = (state: State): SelectState => state["select"];

export const getSelected = (state: State) => {
  const { selected } = getSelectState(state);

  return selected;
};

export const getAllLevels = (state: State) => {
  const { allLevels } = getSelectState(state);

  return allLevels;
};

export const getFilteredLevels = (state: State) => {
  const { filteredLevels } = getSelectState(state);

  return filteredLevels;
};

export const getRawLevels = (state: State) => {
  const { rawLevels } = getSelectState(state);

  return rawLevels;
};
