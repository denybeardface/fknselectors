import { takeLatest, put, select } from "redux-saga/effects";
import {
  getLevels,
  setRawLevels,
  setAllLevels,
  setFilteredLevels,
  setSelected,
} from "./slice";
import { getRawLevels, getSelected } from "./getters";
import { results } from "./data.json";
import { SelectNode, Levels } from "./types";

function dfs(
  start: SelectNode,
  adjList: Map<SelectNode, Array<SelectNode>>,
  visited = new Set(),
  result: Array<SelectNode> = []
) {
  visited.add(start);
  result.push(start);
  const links = adjList.get(start) || []; // just TS things;
  const isRootNode = (node: SelectNode) => node.lvl === 1;

  for (const link of links) {
    if (isRootNode(link)) {
      result.push(link);
      return result;
    }

    if (!visited.has(link)) {
      dfs(link, adjList, visited, result);
    }
  }

  return result;
}

function* loadLevels() {
  const adjList = new Map();

  const addNode = (node: SelectNode) => {
    adjList.set(node, []);
  };

  const addLink = (parent: SelectNode, child: SelectNode) => {
    adjList.get(parent).push(child);
    adjList.get(child).push(parent);
  };

  const buildMap = (item: SelectNode, levels: Levels = []) => {
    const entry = adjList.get(item);
    if (!entry) addNode(item);
    const key = item.lvl - 1;
    const level = levels[key];
    const hasChildren = item.children;

    if (level) {
      level.push(item);
    } else {
      levels[key] = [item];
    }
    if (hasChildren) {
      hasChildren.forEach((child) => {
        addNode(child);
        addLink(item, child);
        buildMap(child, levels);
      });
    }

    return levels;
  };
  // Assuming the only root node, otherwise we're not in the tree I guess ¯\_(ツ)_/¯
  const levels = buildMap(results[0]);
  yield put(setRawLevels(adjList));
  yield put(setAllLevels(levels));
}

const sortByLevel = (nodeA: SelectNode, nodeB: SelectNode): number =>
  nodeA.lvl - nodeB.lvl;

function* filterSelected() {
  const selected = yield select(getSelected);
  const adjList = yield select(getRawLevels);

  if (!selected.length) {
    yield put(setFilteredLevels([]));
    return;
  }

  let startNode: SelectNode = selected[0];

  selected.forEach((node: SelectNode) => {
    startNode = node.lvl < startNode.lvl ? startNode : node;
  });
  const t0 = performance.now();

  console.group("%cSearch", "color: lime");

  const searchRes = dfs(startNode, adjList);
  const t1 = performance.now();

  console.log("Root node found in " + (t1 - t0) + "ms");
  console.log("Map size : " + adjList.size);
  console.groupEnd();

  const filteredLevels = getNestedChildren(searchRes.sort(sortByLevel));
  yield put(setFilteredLevels(filteredLevels));
}

const buildHead = (head: Array<SelectNode>) => {
  console.group("%cBuilding head", "color: skyblue");
  const t0 = performance.now();
  let lvl = 0;

  const levels = head.reduce((acc: Levels, cur: SelectNode): Levels => {
    if (cur.lvl > lvl) {
      lvl = cur.lvl;
      return [...acc, [cur]];
    } else {
      return insertIntoArrayOfArrays(acc, cur);
    }
  }, []);

  const t1 = performance.now();
  console.log("Finished building levels in " + (t1 - t0) + "ms");
  console.groupEnd();
  return levels;
};

const getNestedChildren = (head: Array<SelectNode>) => {
  const [start] = head.slice(-1);

  if (!start.children) {
    return buildHead(head);
  }

  let nestedChildren: Array<Array<SelectNode>> = buildHead(head);
  let lvl = 0;
  const queue: Array<SelectNode> = [start];
  const t0 = performance.now();
  console.group("%cBuilding additional levels", "color: slateblue");

  while (queue.length) {
    const node = queue.shift();
    if (!node) {
      break;
    }

    const { children } = node;
    const isSameNode = node === start;
    const isSameLevel = lvl === node.lvl;

    children && children.map((child: SelectNode) => queue.push(child));

    if (isSameNode) {
      continue;
    }

    if (isSameLevel) {
      nestedChildren = insertIntoArrayOfArrays(nestedChildren, node);
    } else {
      lvl = node.lvl;
      nestedChildren = [...nestedChildren, [node]];
    }
  }
  const t1 = performance.now();
  console.log("Building additional levels finished in " + (t1 - t0) + "ms");
  console.groupEnd();
  return nestedChildren;
};

const insertIntoArrayOfArrays = (
  array: Array<Array<SelectNode>>,
  value: SelectNode
) => {
  const [tail] = array.slice(-1);
  const head = array.slice(0, -1);
  return [...head, [...tail, value]];
};

function* selectWatcher() {
  yield takeLatest(getLevels.type, loadLevels);
  yield takeLatest(setSelected.type, filterSelected);
}

export default [selectWatcher];
