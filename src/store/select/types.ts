export type SelectNode = {
  lvl: number;
  lvl_code: string;
  lvl_name: string;
  children?: Array<SelectNode>;
};

export type Levels = Array<Array<SelectNode>>;

export type SelectState = {
  selected: Array<SelectNode>;
  rawLevels: Map<SelectNode, SelectNode>;
  allLevels: Array<Array<SelectNode>>;
  filteredLevels: Array<Array<SelectNode>>;
  loading: boolean;
};
