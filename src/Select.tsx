import * as React from "react";
import { SelectNode } from "./store/select/types";

type InputProps = {
  children: Array<SelectNode>;
  selected: Array<SelectNode>;
  onSelect: (node: SelectNode) => void;
};

const Select = ({ children, selected, onSelect }: InputProps) => {
  const lvl = children[0].lvl;
  const thisLevelNode = selected.find((item: SelectNode) => item.lvl === lvl);
  const selectedValue = thisLevelNode?.lvl_code || "";
  const handleSelect = (event: any) => {
    const { value } = event.target;
    const selectedNode =
      children && children.find((child) => child.lvl_code === value);
    const stubNode = {
      lvl,
      lvl_code: "",
      lvl_name: "",
    };

    if (selectedNode) {
      onSelect(selectedNode);
    } else {
      onSelect(stubNode);
    }
  };

  return (
    <select onChange={handleSelect} value={selectedValue}>
      <option value={""}>---</option>
      {children &&
        children.map((child: SelectNode, idx: number) => (
          <option key={child.lvl_code + idx}>{child.lvl_code}</option>
        ))}
    </select>
  );
};

export default Select;
