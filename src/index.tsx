import * as React from "react";
import { render } from "react-dom";
import App from "./App";
import { Provider } from "react-redux";
import createStore from "./store/createStore";
import { getLevels } from "./store/select/slice";
const store = createStore();

store.dispatch(getLevels());

const rootElement = document.getElementById("root");
render(
  <Provider store={store}>
    <App />
  </Provider>,
  rootElement
);
